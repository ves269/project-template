# Simple CMAKE project template

This project template is current only for designed for executable projects. Support for libraries will be added later.

## Project structure

```go
project_template/
├── cmake/
│   ├── basics.cmake
│   ├── codeAnalysisTools.cmake
│   ├── codeDocumentationTools.cmake
│   ├── codeFormat.cmake
│   ├── compilerOptimizationTools.cmake
│   ├── fetchCatch2.cmake
│   └── runtimeChecks.cmake
├── doc/
│   ├── img/
│       └── logo.png
├── inc/
├── src/
│   ├── CMakeList.txt
│   └── main.cpp
├── test/
│   ├── CMakeList.txt
│   └── test_main.cpp
├── .clang-format
├── .gitignore
├── CMakeList.txt
├── CMakePresets.json
└── README.md
```

Customized CMake functions are located in the `cmake` folder and have the file extension `.cmake`.
Collection folder for all includes files in the `inc` folder.
Collection folder for all source files in the `src` folder.
The control is carried out via the `CMakeList.txt` file in the `src` folder for all source files (must be entered manually here).
Collection folder for all unit test files in the `test` folder.
The control is carried out via the `CMakeList.txt` file in the `test`folder for all test files (must be entered manually here).
The file `.clang-format` contains the rules of the Code Style Guide.
The `CMakeLists.txt` in the main folder is the main CMAKE configuration file (this is where the software versioning is controlled, for example).

Functions can be specifically activated or deactivated in `CMakePresets.json`. For Example:

```json
      "cacheVariables": {
        "ENABLE_COMPILER_OPTIMIZATION": "OFF",
        "ENABLE_COMPILER_OPTIMIZATION_MAX": "OFF",
        "ENABLE_INTERPROCEDURAL_OPTIMIZATION": "OFF",
        "ENABLE_CLANG_FORMAT": "ON",
        "ENABLE_SANITIZER": "ON",
        "ENABLE_COMPILER_WARNINGS": "ON",
        "ENABLE_COMPILER_WARNINGS_AS_ERRORS": "OFF",
        "ENABLE_CLANG_TIDY_DEFAULT_CHECKS": "ON",
        "ENABLE_CATCH2": "ON",
        "ENABLE_DOXYGEN": "ON"
      }
```
