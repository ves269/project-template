#include <array>
#include <iostream>
#include <projectInfo>

using namespace ves269;

void waitForEnter()
{
	std::cout << "Press Enter to continue...";
	std::cin.get();
}

int main()
{
	std::cout << "Project name:         " << kProjectName << "\n";
	std::cout << "Project description:  " << kProjectDescription << "\n";
	std::cout << "HomepageUrl:          " << kHomepageUrl << "\n";
	std::cout << "Organization name:    " << kOrganizationName << "\n\n";

	const std::array version{ kProjectVersionMajor, kProjectVersionMinor, kProjectVersionPatch, kProjectVersionTweak };

	std::cout << "Project version: ";

	for( auto it = version.cbegin(); it != version.cend(); ++it )
	{
		std::cout << *it;

		if( it < version.cend() - 1 ) { std::cout << "."; }
	}
	std::cout << "\n\n";

	waitForEnter();

	return 0;
}
