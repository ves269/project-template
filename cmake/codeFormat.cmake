# --------------------------------------------------------------------------------
# Code style format options
# --------------------------------------------------------------------------------

# Add clang-tidy default checks to the project.
function(enable_clang_format target files_format)
    if(${CMAKE_BUILD_TYPE} MATCHES "Release")
        message(STATUS "Clang format option only make sense in debug mode!")
        return()
    endif()
        message(STATUS "Clang-format files:")
        foreach( ARG ${files_format})
            message(STATUS "-: ${ARG}")
        endforeach()

        add_custom_command(TARGET ${target} PRE_BUILD
                           COMMAND clang-format -i ${files_format}
                           COMMENT "Running clang-format as pre-build step")
        
        message(STATUS "Clang-format option is enabled")
endfunction()