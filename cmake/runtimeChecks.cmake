# --------------------------------------------------------------------------------
# Runtime check options (or Sanitizer options)
# --------------------------------------------------------------------------------

# Add sanitizer option to the project.
function(enable_sanitizer target)
    if(${CMAKE_BUILD_TYPE} MATCHES "Release")
        message(STATUS "The sanitizer option only make sense in debug mode!")
        return()
    endif()

    if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
        target_compile_options(${target} PRIVATE "/fsanitize=address")
        # /fsanitize=fuzzer may not be necessary, depending on your use case
        #target_compile_options(${target} PRIVATE "/fsanitize=fuzzer")
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR CMAKE_CXX_COMPILER_ID MATCHES "GNU")
       target_compile_options(${target} PRIVATE "-fno-omit-frame-pointer")
       target_link_options(${target} PRIVATE "-fno-omit-frame-pointer")

       target_compile_options(${target} PRIVATE "-fsanitize=address")
       target_link_options(${target} PRIVATE "-fsanitize=address")

       target_compile_options(${target} PRIVATE "-fsanitize=undefined")
       target_link_options(${target} PRIVATE "-fsanitize=undefined")

       target_compile_options(${target} PRIVATE "-fsanitize=leak")
       target_link_options(${target} PRIVATE "-fsanitize=leak")
    endif()

     message(STATUS "Sanitizer options are enabled")
endfunction()