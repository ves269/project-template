# --------------------------------------------------------------------------------
# Code analysis tool options
# --------------------------------------------------------------------------------

# Add compiler warnings to the project.
function(enable_compiler_warnings target)
    if(${CMAKE_BUILD_TYPE} MATCHES "Release")
        message(STATUS "Compiler warnings only useful in debug mode!")
        return()
    endif()

    if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
        string(REPLACE "/W3" "" NEW_CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
        set(CMAKE_CXX_FLAGS ${NEW_CMAKE_CXX_FLAGS} PARENT_SCOPE)

        target_compile_options(${target} PRIVATE /W4)
        target_compile_options(${target} PRIVATE /permissive-)
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR CMAKE_CXX_COMPILER_ID MATCHES "GNU")
        target_compile_options(${target} PRIVATE -Wall)
        target_compile_options(${target} PRIVATE -Wextra)
        target_compile_options(${target} PRIVATE -Wpedantic)
    endif()

    message(STATUS "Compiler warnings are enabled")
endfunction()

# Add compiler warnings as errors to the project.
function(enable_compiler_warnings_as_errors target)
    if(${CMAKE_BUILD_TYPE} MATCHES "Release")
        message(STATUS "Compiler warnings as errors only useful in debug mode!")
        return()
    endif()
    
    if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
        target_compile_options(${target} PRIVATE /WX)
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR CMAKE_CXX_COMPILER_ID MATCHES "GNU")
        target_compile_options(${target} PRIVATE -Werror)
    endif()

    message(STATUS "Compiler warnings as errors are enabled")
endfunction()

# Add clang-tidy default checks to the project.
function(enable_clang_tidy_default_checks target clang_tidy_checks)
    if(${CMAKE_BUILD_TYPE} MATCHES "Release")
        message(STATUS "Clang tidy checks only make sense in debug mode!")
        return()
    endif()
        if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
            set_target_properties( ${target} PROPERTIES CXX_CLANG_TIDY "clang-tidy;-header-filter=^((?!_debs).)*$;-checks=${clang_tidy_checks};--extra-arg=/EHsc" )
        else()
            set_target_properties( ${target} PROPERTIES CXX_CLANG_TIDY "clang-tidy;-header-filter=^((?!_debs).)*$;-checks=${clang_tidy_checks}" )
        endif()
        
        message(STATUS "Clang-tidy default checks are enabled")
endfunction()