# --------------------------------------------------------------------------------
# Code documentation tools
# --------------------------------------------------------------------------------

# Search for Doxygen and add it to the project if it is supported.
function(enable_doxygen_support)
    find_package(Doxygen REQUIRED dot OPTIONAL_COMPONENTS mscgen dia)
    if(DOXYGEN_FOUND)
        set(target_name "documentation")
        # Setup doxygen
        # - Defaults
        set(DOXYGEN_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${target_name})
        set(DOXYGEN_PROJECT_BRIEF ${ORGANIZATION_NAME})
        set(DOXYGEN_PROJECT_LOGO "${CMAKE_CURRENT_SOURCE_DIR}/doc/img/logo.png")
        set(DOXYGEN_OUTPUT_LANGUAGE "English")
        set(DOXYGEN_ALLOW_UNICODE_NAMES YES)
        set(DOXYGEN_CREATE_SUBDIRS YES)
        set(DOXYGEN_MARKDOWN_SUPPORT YES)
        set(DOXYGEN_EXTRACT_ALL YES)
        set(DOXYGEN_EXTRACT_PRIVATE YES)
        set(DOXYGEN_EXTRACT_STATIC YES)
        set(DOXYGEN_JAVADOC_AUTOBRIEF YES)
        set(DOXYGEN_QT_AUTOBRIEF YES)
        set(DOXYGEN_INLINE_SOURCES YES) # Displays source code
        set(DOXYGEN_WARN_LOGFILE ${CMAKE_CURRENT_BINARY_DIR}/${target_name}/doxygen.log)
        set(DOXYGEN_IMAGE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/doc/img)
        set(DOXYGEN_USE_MDFILE_AS_MAINPAGE ${CMAKE_CURRENT_SOURCE_DIR}/README.md)
        # Navigation
        set(DOXYGEN_DISABLE_INDEX YES)
        set(DOXYGEN_GENERATE_TREEVIEW YES)
        set(DOXYGEN_FULL_SIDEBAR NO)
        # - HTML Parameter
        set(DOXYGEN_GENERATE_HTML YES)
        set(DOXYGEN_HTML_DYNAMIC_MENUS YES)
        set(DOXYGEN_HTML_DYNAMIC_SECTIONS YES)
        set(DOXYGEN_HTML_HTML_CODE_FOLDING YES)
        set(DOXYGEN_HTML_COLORSTYLE TOGGLE)
        set(DOXYGEN_HTML_COLORSTYLE_SAT 0)
        # - Dot Parameter
        if(TARGET Doxygen::dot)
            set(DOXYGEN_CALL_GRAPH YES)
            set(DOXYGEN_CALLER_GRAPH YES)
            set(DOXYGEN_INTERACTIVE_SVG YES)

            set(DOXYGEN_UML_LOOK YES)
        endif()

        set(documentation_dir "${PROJECT_SOURCE_DIR}/documentation")

        # Run option
        doxygen_add_docs(${target_name}
                ${CMAKE_CURRENT_BINARY_DIR}/src
                ${CMAKE_CURRENT_SOURCE_DIR}/src
                ${CMAKE_CURRENT_SOURCE_DIR}/README.md
                COMMENT "Generating documentation")

        if(EXISTS "${documentation_dir}")
            add_custom_command(TARGET ${target_name} POST_BUILD
                               COMMAND ${CMAKE_COMMAND} -E rm -rf  "${documentation_dir}"
                               COMMENT "Remove the temporary documentation directory: ${documentation_dir}")
        endif()

        if(NOT EXISTS "${documentation_dir}")
            add_custom_command(TARGET ${target_name} POST_BUILD
                               COMMAND ${CMAKE_COMMAND} -E make_directory  "${documentation_dir}"
                               COMMENT "Creating the documentation directory: ${documentation_dir}")
        endif()

        add_custom_command(TARGET ${target_name} POST_BUILD
                           COMMAND ${CMAKE_COMMAND} -E copy_directory "${DOXYGEN_OUTPUT_DIRECTORY}/html" "${documentation_dir}"
                           COMMAND ${CMAKE_COMMAND} -E rm -rf "${DOXYGEN_OUTPUT_DIRECTORY}/html"
                           COMMENT "Move html documentation to ${documentation_dir}")

        message(STATUS "Doxygen support is enabled")
    else ()
        message(STATUS "Doxygen support is not supported")
    endif ()
endfunction()