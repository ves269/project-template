# --------------------------------------------------------------------------------
# Compiler optimization tool options
# --------------------------------------------------------------------------------

# Add compiler optimization to the project.
function(enable_compiler_optimization)
    if(${CMAKE_BUILD_TYPE} MATCHES "Debug")
        message(STATUS "The compiler optimization only make sense in release mode!")
        return()
    endif()

    if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
        add_compile_options(/O2)
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        add_compile_options(-O2)
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
        add_compile_options(-O2)
    endif()

    message(STATUS "Compiler optimization are enabled")
endfunction()

# Add maximum compiler optimization to the project.
function(enable_compiler_optimization_maximum)
    if(${CMAKE_BUILD_TYPE} MATCHES "Debug")
        message(STATUS "The maximum compiler optimization only make sense in release mode!")
        return()
    endif()

    if(CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
        add_compile_options(/Ox)
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        add_compile_options(-O3)
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "GNU")
        add_compile_options(-O3)
    endif()

    message(STATUS "Maximum compiler optimization are enabled")
endfunction()

# Search for interprocedural optimization option (IPO/LTO) and add it to the project if is supported.
function(enable_interprocedural_optimization)
    if(${CMAKE_BUILD_TYPE} MATCHES "Debug")
        message(STATUS "The interprocedural optimization option (IPO/LTO) only make sense in release mode!")
        return()
    endif()

    include(CheckIPOSupported)
    check_ipo_supported(RESULT result OUTPUT output)

    if(result) 
        set(CMAKE_INTERPROCEDURAL_OPTIMIZATION ON PARENT_SCOPE)
        message(STATUS "Interprocedural optimization is enabled")
     else()
        message(STATUS "Interprocedural optimization is not supported: ${output}")
    endif()
endfunction()