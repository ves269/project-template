# --------------------------------------------------------------------------------
# Catch2 Unit Test-Framework
# --------------------------------------------------------------------------------
include(FetchContent)

# Fetch catch2 the unit test-framework.
function(fetch_catch2)
	FetchContent_Declare(
		Catch2
		GIT_REPOSITORY https://github.com/catchorg/Catch2.git
		GIT_TAG v3.5.2
		GIT_SHALLOW TRUE)

	FetchContent_MakeAvailable(Catch2)

	set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} PARENT_SCOPE)
endfunction()