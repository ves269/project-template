#include <catch2\catch_all.hpp>

#include <projectInfo>
// #include "YOUR_HEADER"

TEST_CASE( "Check anything" )
{
	SECTION( "First Section" )
	{
		REQUIRE( 1 + 1 > 1 );
		REQUIRE( 10 * 2 >= 20 );
	}

	SECTION( "Second Section" )
	{
		REQUIRE( std::string("TestString") == std::string("TestString") );
		REQUIRE( ves269::kOrganizationName == std::string("ves269") );
	}
}
